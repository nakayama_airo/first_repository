package ex.jp.alhinc.nakayama_airo.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class CalculateSales {
	public static void main(String[] args) { 
        System.out.println("ここにあるファイルを開きます =>" + args[0]);

		TreeMap<String, String> branchMap = new TreeMap<String, String>();

		//  以降、branch.lstの作成。

		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {            // line <- 001,札幌支店
				//System.out.println(line);
		     if (!line.isEmpty()) {	 
				String[] codeBranch = line.split(",");         //001,札幌支店 
				branchMap.put(codeBranch[0], codeBranch[1]);
		     }
			}
		} catch (IOException e) {
			System.out.println("エラーが発生しました。");
		} finally {
			if (br != null) {
				try {
					br.close();
			    } catch(IOException e)	{
				    System.out.println("closeできませんでした。");
				}
			}
		}

		File dir = new File(args[0]);
		File[] files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(".rcd");
			}
		});

		// 以降、rcdの作成。

		Map<String, Integer> totalMap = new HashMap<String, Integer>();

		BufferedReader rcdBr = null;
		for(int i = 0; i < files.length; i++) {
			try {
				File file = new File(args[0], files[i].getName());
				FileReader fr = new FileReader(file);
				rcdBr = new BufferedReader(fr);

				String line;
				String code = "";  // " "
				int sales = 0;
				int lineNumber = 0;

				while((line = rcdBr.readLine()) != null) { // line <- 001,札幌支店
					if (lineNumber  == 0) {
						code = line;
					  } else if (lineNumber == 1) {
						  sales = Integer.parseInt(line);
					  }
					  lineNumber++;
				  }
				  if (!code.isEmpty()) {
					  if (totalMap.containsKey(code)) {
						  int oldToal = totalMap.get(code);
						  totalMap.put(code, oldToal + sales);
					  } else {
						  totalMap.put(code, sales);
					  }     
				  }
                

		      } catch (IOException e) {
			      System.out.println("エラーが発生しました。");
		      } finally {
			      if (rcdBr != null) {
				      try {
					      rcdBr.close();
				      } catch(IOException e) {
					      System.out.println("closeできませんでした。");
				      }
			      }
	          }
          }

          // 以降、outputの作成。

          BufferedWriter writer = null;
          try {
        	  File file = new File(args[0], "branch.out");
        	  FileWriter fw = new FileWriter(file);
        	  writer = new BufferedWriter(fw);
        
        	  for (String code : branchMap.keySet()) {
        		  int total = 0;
        		  if (totalMap.containsKey(code)) {
        			  total = totalMap.get(code);
        		  }
        		  String branchName = branchMap.get(code);
        		  String Line = String.format("%s,%s,%d",code, branchName, total);
        		  writer.write(Line);
        		  writer.newLine();
        		  System.out.println(Line);
        	 }

         } catch (IOException e) {
        	 System.out.println("エラーが発生しました。");
         } finally {
        	 if (writer != null) {
        		 try {
        			 writer.close();
        		 } catch(IOException e) {
        			 System.out.println("closeできませんでした。");
        		 }
             }
         }
	}
}